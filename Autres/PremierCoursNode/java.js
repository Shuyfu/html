import React from 'react'
import ReactDom from 'react-dom'

function Header(props) {
    return (
        <header>
            <h1>{props.title}</h1>
        </header>
    )
}

function ShoppingList(props) {
    const shoppingElements = props.shoppingItems.map((item, i) => {
        return (
            <li key={i}>
                <span>{item.quantity}</span> {item.title}
                <button onClick={()=> props.removeItem(i)}>X</button>
            </li>
        )
    })

    return (
        <ul>
            {shoppingElements}
        </ul>
    )
}

// return (
//     <ul>
//         {shoppingItems.map((item, i) =>
//         <li key={i}>
//             <span>{item.quantity}</span> {item.title}
//         </li>
// )}
//     </ul>
// )

function AddShoppingItem(props) {
    const [quantity, setQuantity] = React.useState("")
    const [title, setTitle] = React.useState("")

    function changeQuantity(event){
        setQuantity(event.target.value)
    }

    function changeTitle(event){
        setTitle(event.target.value)
    }

    function handleClick(){
        console.log(quantity, title);
        const item = {quantity: quantity, title: title}
        props.addItem(item)
    }

    const isDisabled = quantity.length < 1 || title.length < 3

    return (
    <>
        <input type="text" value={quantity} onChange={changeQuantity} placeholder="quantité" />
        <input type="text" value={title} onChange={changeTitle} placeholder="ingredient" />
        <button onClick={handleClick} disabled={isDisabled}>AJOUTER</button>
    </>
    )
}

function App(props) {
    const [items, updateItem] =
        React.useState([
            { quantity: "1kg", title: "choux" }
        ])

    function addItem(item){
        let newItems = [...items, item]
        updateItem(newItems)
    }

    function removeItem(index){
        
        // const newItems = []
        // for (let i = 0; i < items.length; i++){
        //     if(i === index) continue
        //     newItems.push(items[i])
        // }

        const newItems = items.filter((item, i) => i != index)
        updateItem(newItems)
    }

    return (
        <section className="main">
            <Header title="Mes courses" />
            <article>
                <ShoppingList shoppingItems={items} removeItem={removeItem} />
            </article>
            <footer>
                <AddShoppingItem addItem={addItem}/>
            </footer>
        </section>
    )
}

ReactDom.render(
    <App />,
    document.getElementById('app')
)