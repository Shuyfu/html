console.log('Hello Node !')
const http = require('http')
const data = require('./data.js')

const qcms = [
    { 
        question: "Qu'est ce que le HTTP ?",
        response: ["A", "B", "C"]
    }
]

const server = http.createServer(function(req, res){
    console.log(req.url)
    if (req.url == '/people'){
        res.setHeader('Access-Control-Allow-Origin', '*')
        res.setHeader('Content-Type', 'application/json')
        const data = JSON.stringify(qcms)
        res.end(data)
        return
    }
    if (req.url == 'css'){
        //res.end(...)
        return
    }
    
})

server.listen(8000, '0.0.0.0');
