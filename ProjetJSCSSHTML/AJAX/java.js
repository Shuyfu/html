
let newButton = document.createElement('button');
let nomButton = document.createTextNode('Requete');
newButton.appendChild(nomButton);
newButton.addEventListener('click',loadPage);
document.body.appendChild(newButton);

function loadPage() 
{
  let newDiv = document.createElement('div');
  newDiv.id = 'demo';
  document.body.appendChild(newDiv);

  let i = 0;

  while(i<10)
  {
    let req = new XMLHttpRequest() 
    let j = i;

    req.onreadystatechange = function() 
    {
      if (this.readyState == 4 && this.status == 200) 
      {
        let personnage = JSON.parse(this.responseText);

        newBranch(j, personnage);

        console.log(this.responseText);
        console.log(personnage);
      }
    }

    req.open("GET", `https://swapi.co/api/people/${i+1}/`, true); 
    req.send();
    i++;
  }
}

function newBranch(j, personnage)
{
  let newUl = document.createElement('ul');
  newUl.className = 'perso';
  newUl.id = `id${j}`;
  document.getElementById('demo').appendChild(newUl);

  let newLi = document.createElement('li');
  newLi.className = 'nom';
  newLi.innerHTML = personnage.name;
  newUl.appendChild(newLi);

  newLi = document.createElement('li');
  newLi.className = 'naissance';
  newLi.innerHTML = personnage.birth_year;
  newUl.appendChild(newLi);

  newLi = document.createElement('li');
  newLi.className = 'yeux';
  newLi.innerHTML = personnage.eye_color;
  newUl.appendChild(newLi);
}
